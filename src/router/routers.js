import homePage from '../components/homePage'
import aboutPage from '../components/aboutPage'
import productsPage from '../components/productsPage'
import adminPage from '../components/adminPage'


const routes = [
    {
        path: '/',
        name: 'home',
        component: homePage
    },
    {
        path: '/aboutPage',
        name: 'about',
        component: aboutPage
    },
    {
        path: '/productsPage',
        name: 'products',
        component: productsPage
    },
    {
        path: '/adminPage',
        name: 'adminPage',
        component: adminPage
    }

]


export default routes