import Vue from 'vue'
import App from './App.vue'
import router from './router'
import headerNavbar from "./components/headerNavbar"
import footerNavbar from"./components/footerNavbar"
import VueBus from 'vue-bus';
import Vuelidate from 'vuelidate'
import store from './store';

Vue.use(Vuelidate);

Vue.use(VueBus);
Vue.config.productionTip = false
Vue.component("headerNavbar",headerNavbar)
Vue.component("footerNavbar",footerNavbar)

new Vue({
  render: h => h(App),
    router,
    store
}).$mount('#app')
