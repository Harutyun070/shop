export default {
    state: {
        cart: [],
    },
    mutations: {
        createState(state,newProduct){
            state.cart = newProduct
        },
        deleteState(state,newProduct){
            state.cart = newProduct
            console.log(state.cart)
        }
    },
    actions: {
        fetchState(ctx){
            let arr = localStorage.getItem("cart")
            let data = JSON.parse(arr) ? JSON.parse(arr) : []
            ctx.commit('createState', data)
        },
        deleteLocal(ctx){
            localStorage.clear()
            let data = this.state.cart = []
            ctx.commit('deleteState',data)
        }
    },
    getters: {
        allState(state){
            return state.cart
        }
    }
}
