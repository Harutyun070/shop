import Vue from 'vue';
import Vuex from 'vuex';
import products from './products';
import data from './data.js'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        products,
        data,
    }
})