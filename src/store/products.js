export default {
    state: {
        products: [
            {
                name: 'verx',
                color: 'green',
                count: 1,
                price: 5000,
                description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
            },
            {
                name: 'shalvar',
                count: 1,
                color: 'black',
                price: 8000,
                description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
            }
        ],
    },
    getters: {
        allProducts(state){
            return state.products
        }
    },
    mutations:{
        createProducts(state,newProducts){
        state.products.push(newProducts)
        },
    },
    actions: {
        fetchProducts(ctx,data){
            ctx.commit('createProducts',data)
        }
    }

}